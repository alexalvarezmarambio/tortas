import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TortasService {

  constructor(private firebase: AngularFirestore) { }

  select(coleccion: string) {



    // this.firebase.collection('ingredientes').get().subscribe( snapshot => {
    //   snapshot.forEach( doc => {
    //     console.log(doc.data());
    //   } );
    // });

    // return this.firebase.collection('ingredientes').valueChanges().pipe( tap( console.log ));

    return this.firebase.collection(coleccion).snapshotChanges().pipe( map( snapshot => {

      const objetos: any[] = [];

      snapshot.forEach( doc => {
        const data: any = doc.payload.doc.data();
        objetos.push( {
          id: doc.payload.doc.id,
          ...data
        });
      });

      return objetos;
    } ));

    // this.firebase.collection('ingredientes').stateChanges().subscribe( console.log );

  }

  insert(coleccion: string, objeto: any) {

    this.firebase.collection(coleccion).add(objeto);
  }

  update(coleccion: string, id: string, objeto: any) {
    this.firebase.doc(`${coleccion}/${id}`).update(objeto);
  }

  delete(coleccion: string, id: string) {

    this.firebase.doc(`${coleccion}/${id}`).delete();
  }

  selectId(coleccion: string, id: string) {

    return this.firebase.doc(`${coleccion}/${id}`).get().pipe( map( snap => {
      return snap.data();
    }));
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ingrediente } from 'src/app/interfaces/ingrediente.interface';
import { TortasService } from 'src/app/tortas.service';

@Component({
  selector: 'app-ingrediente',
  templateUrl: './ingrediente.component.html',
  styles: [
  ]
})
export class IngredienteComponent implements OnInit {

  id: any;
  titulo: string;
  ingrediente: Ingrediente;

  constructor(private activatedRoute: ActivatedRoute,
              public servicio: TortasService,
              private router: Router) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe( params => {
      this.id = params['id'];

      this.ingrediente = {
        nombre: '',
        cantidad: 0,
        medida: '',
        costo: 0
      };

      if (this.id === 'nuevo') {
        this.titulo = 'Nuevo';


      } else {
        this.titulo = 'Editar';
        this.selectId(this.id);
      }
    });
  }

  guardar() {

    if (this.id === 'nuevo') {

      this.servicio.insert('ingredientes', this.ingrediente);
      this.router.navigateByUrl('/ingredientes');

    } else {
      this.servicio.update('ingredientes', this.id, this.ingrediente);
      this.router.navigateByUrl('/ingredientes');
    }

  }

  selectId(id: string) {
    this.servicio.selectId('ingredientes', id).subscribe( snap => {
      this.ingrediente.nombre = snap.nombre;
      this.ingrediente.cantidad = snap.cantidad;
      this.ingrediente.medida = snap.medida;
      this.ingrediente.costo = snap.costo;

    });
  }

}

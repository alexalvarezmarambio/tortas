import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IngredientesComponent } from './ingredientes/ingredientes.component';
import { RecetasComponent } from './recetas/recetas.component';
import { IngredienteComponent } from './ingrediente/ingrediente.component';


@NgModule({
  declarations: [
    IngredientesComponent,
    RecetasComponent,
    IngredienteComponent
  ],
  exports: [
    IngredientesComponent,
    RecetasComponent,
    IngredienteComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class ComponentsModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TortasService } from 'src/app/tortas.service';
import { Observable } from 'rxjs';
import { Ingrediente } from 'src/app/interfaces/ingrediente.interface';

@Component({
  selector: 'app-ingredientes',
  templateUrl: './ingredientes.component.html',
  styles: [
  ]
})
export class IngredientesComponent implements OnInit {

  ingredientes: Observable<Ingrediente[]>;

  constructor(public service: TortasService,
              private router: Router) { }

  ngOnInit(): void {
    this.selectIngredientes();
  }

  selectIngredientes() {
    this.ingredientes = this.service.select('ingredientes');
  }

  goToIngrediente(id: any) {
    this.router.navigateByUrl(`/ingrediente/${id}`);
  }

  deleteIngrediente(id: string) {
    const aux = confirm('Seguro desea eliminar este Ingrediente?');
    if (aux) {
      this.service.delete('ingredientes', id);
    }
  }



}

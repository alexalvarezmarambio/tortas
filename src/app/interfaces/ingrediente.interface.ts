export interface Ingrediente {

    id?: string;
    nombre: string;
    cantidad: number;
    medida: string;
    costo: number;


}
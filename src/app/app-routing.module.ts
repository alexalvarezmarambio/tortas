import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecetasComponent } from './components/recetas/recetas.component';
import { IngredientesComponent } from './components/ingredientes/ingredientes.component';
import { IngredienteComponent } from './components/ingrediente/ingrediente.component';

const routes: Routes = [
  {path: '', redirectTo: 'recetas', pathMatch: 'full'},
  {path: 'recetas', component: RecetasComponent},
  {path: 'ingredientes', component: IngredientesComponent},
  {path: 'ingrediente/:id', component: IngredienteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAZAQ8yhO25J_XZ93JR3Z4yKLl9A6A07yA",
    authDomain: "costos-pasteleria.firebaseapp.com",
    databaseURL: "https://costos-pasteleria.firebaseio.com",
    projectId: "costos-pasteleria",
    storageBucket: "costos-pasteleria.appspot.com",
    messagingSenderId: "849952368230",
    appId: "1:849952368230:web:6d56c13f1d8493db892fa7",
    measurementId: "G-QKZ8J4G1QM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
